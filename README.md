# acrylic_polymers

## ML glass transition temperature
Repository for 'Determining glass transition in all-atom acrylic polymeric melts
simulations using machine learning' https://XXXXXXX (Doi will be added)

## Download the DATA

Please download the homopolymer data from this link https://doi.org/10.17617/3.4JHOMW

* There are three homopolymers : PMMA, PEA, PnBA used in this project
* Each system has gromacs trajectory xtc files at 11 different temperatures : '600','550','500','450', '400', '350', '300', '250', '200', '150', '100' (in Kelvin)
* Download the topology file for each system 

Please Cite the dataset as: 
---
@data{3.4JHOMW_2023,\
author = Iscen Akatay, Aysenur,\
publisher = Edmond,\
title = Simulation trajectories for ``Acrylic Paints: An Atomistic View of the Polymer Structure and Effects of Environmental Pollutants'',\
year = 2023,\
version = V1,\
doi = 10.17617/3.4JHOMW,\
url = https://doi.org/10.17617/3.4JHOMW

---

## Test distance data for PMMA 
As a test case, all the distance files for PMMA systems are given in the npz_files.7z folder (in zip format)
 
